// MessageSender.java - PARTIAL IMPLEMENTATION

/**
 * This class implements the sender side of the data link layer.
 * <P>
 * The source code supplied here only contains a partial implementation. 
 * Your completed version must be submitted for assessment.
 * <P>
 * You only need to finish the implementation of the sendMessage
 * method to complete this class.  No other parts of this file need to
 * be changed.  Do NOT alter the constructor or interface of any public
 * method.  Do NOT put this class inside a package.  You may add new
 * private methods, if you wish, but do NOT create any new classes. 
 * Only this file will be processed when your work is marked.
 */

public class MessageSender
{
    // Fields ----------------------------------------------------------

    private int mtu;                    // maximum transfer unit (frame length limit)
    private FrameSender physicalLayer;  // physical layer object
    private TerminalStream terminal;    // terminal stream manager

    // DO NOT ADD ANY MORE INSTANCE VARIABLES
    // but it's okay to define constants here

    // Constructor -----------------------------------------------------

    /**
     * MessageSender constructor - DO NOT ALTER ANY PART OF THIS
     * Create and initialize new MessageSender.
     * @param mtu the maximum transfer unit (MTU)
     * (the length of a frame must not exceed the MTU)
     * @throws ProtocolException if error detected
     */

    public MessageSender(int mtu) throws ProtocolException
    {
        // Initialize fields
        // Create physical layer and terminal stream manager

        this.mtu = mtu;
        this.physicalLayer = new FrameSender();
        this.terminal = new TerminalStream("MessageSender");
        terminal.printlnDiag("data link layer ready (mtu = " + mtu + ")");
    }

    // Methods ---------------------------------------------------------

    /**
     * Send a message - THIS IS THE ONLY METHOD YOU NEED TO MODIFY
     * @param message the message to be sent.  The message can be any
     * length and may be empty but the string reference should not
     * be null.
     * @throws ProtocolException immediately without attempting to
     * send any further frames if, and only if, the physical layer
     * throws an exception or the given message can't be sent
     * without breaking the rules of the protocol including the MTU
     */

    public void sendMessage(String message) throws ProtocolException
    {
        // Report action to terminal
        // Note the terminal messages aren't part of the protocol,
        // they're just included to help with testing and debugging

        terminal.printlnDiag("  sendMessage starting (message = \"" + message + "\")");

        // YOUR CODE SHOULD START HERE ---------------------------------
        // No changes are needed to the statements above


        final int minMtu = 8;
        final int messageSegLenMax = mtu - 8;
        String messageCpy = message;

        if (mtu < minMtu)
            throw new ProtocolException("mtu too small");

        messageCpy = messageCpy.replaceAll(":", "::");

//        terminal.printlnDiag("messageSegLenMax: " + messageSegLenMax);

        if (messageCpy.length() == 0)
            physicalLayer.sendFrame("(:000:.)");
        else
            while (messageCpy.length() > 0) {

                String messageSeg;
                if (messageCpy.length() > messageSegLenMax) {
                    messageSeg = messageCpy.substring(0, messageSegLenMax);

                    int colonCount = 0;
                    for (int c = 0; c < messageSeg.length(); c++)
                        if(messageSeg.charAt(c) == ':')
                            colonCount++;
                    if (messageSeg.charAt(messageSeg.length() - 1) == ':'
                            && (colonCount & 1) != 0) {
                        messageSeg = messageSeg.substring(0, messageSeg.length() - 1);
                        messageCpy = messageCpy.substring(messageSegLenMax - 1);
                    } else {
                        messageCpy = messageCpy.substring(messageSegLenMax);
                    }
                } else {
                    messageSeg = messageCpy;
                    messageCpy = "";
                }

                int checksum = 0;
                String messageSegCpy = messageSeg.replaceAll("::", ":");
                for (int c = 0; c < messageSegCpy.length(); c++)
                    checksum += messageSegCpy.charAt(c);

                String checksumStr = "" + checksum;
                for (int i = checksumStr.length(); i < 3; i++)
                    checksumStr = "0" + checksumStr;
                checksumStr = checksumStr.substring(checksumStr.length() - 3);

                String frame = "(";
                frame += messageSeg;
                frame += ":";
                frame += checksumStr;
                frame += ":";
                if (messageCpy.length() <= 0)
                    frame += ".";
                else
                    frame += "+";
                frame += ")";

    //            terminal.printlnDiag("messageSeg.length(): " + messageSeg.length());

                if (frame.length() <= mtu)
                    physicalLayer.sendFrame(frame);
                else
                    throw new ProtocolException("frame too long");
            }


        // YOUR CODE SHOULD FINISH HERE --------------------------------
        // No changes are needed to the statements below

        // Report completion of task

        terminal.printlnDiag("  sendMessage finished");

    } // end of method sendMessage

    // You may add private methods if you wish


} // end of class MessageSender

