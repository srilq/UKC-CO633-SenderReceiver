// MessageReceiver.java - PARTIAL IMPLEMENTATION

/**
 * This class implements the receiver side of the data link layer.
 * <P>
 * The source code supplied here only contains a partial implementation.
 * Your completed version must be submitted for assessment.
 * <P>
 * You only need to finish the implementation of the receiveMessage
 * method to complete this class.  No other parts of this file need to
 * be changed.  Do NOT alter the constructor or interface of any public
 * method.  Do NOT put this class inside a package.  You may add new
 * private methods, if you wish, but do NOT create any new classes. 
 * Only this file will be processed when your work is marked.
 */

public class MessageReceiver
{
    // Fields ----------------------------------------------------------

    private int mtu;                      // maximum transfer unit (frame length limit)
    private FrameReceiver physicalLayer;  // physical layer object
    private TerminalStream terminal;      // terminal stream manager

    // DO NOT ADD ANY MORE INSTANCE VARIABLES
    // but it's okay to define constants here

    // Constructor -----------------------------------------------------

    /**
     * MessageReceiver constructor - DO NOT ALTER ANY PART OF THIS
     * Create and initialize new MessageReceiver.
     * @param mtu the maximum transfer unit (MTU)
     * (the length of a frame must not exceed the MTU)
     * @throws ProtocolException if error detected
     */

    public MessageReceiver(int mtu) throws ProtocolException
    {
        // Initialize fields
        // Create physical layer and terminal stream manager

        this.mtu = mtu;
        this.physicalLayer = new FrameReceiver();
        this.terminal = new TerminalStream("MessageReceiver");
        terminal.printlnDiag("data link layer ready (mtu = " + mtu + ")");
    }

    // Methods ---------------------------------------------------------

    /**
     * Receive a message - THIS IS THE ONLY METHOD YOU NEED TO MODIFY
     * @return the message received, or null if the end of the input
     * stream has been reached.  See receiveFrame documentation for
     * further explanation of how the end of the input stream is
     * detected and handled.
     * @throws ProtocolException immediately without attempting to
     * receive any further frames if any error is detected, such as
     * a corrupt frame, even if the end of the input stream has also
     * been reached (i.e. signalling an error takes precedence over
     * signalling the end of the input stream)
     */

    public String receiveMessage() throws ProtocolException
    {
        String message = "";    // whole of message as a single string
                                // initialise to empty string

        // Report action to terminal
        // Note the terminal messages aren't part of the protocol,
        // they're just included to help with testing and debugging

        terminal.printlnDiag("  receiveMessage starting");

        // YOUR CODE SHOULD START HERE ---------------------------------
        // No changes are needed to the statements above


        boolean eom;
        do {
            String frame = physicalLayer.receiveFrame();
            int frameLength = frame.length();

            if (frameLength > mtu)
                throw new ProtocolException("frame exceeds mtu: " + mtu);
            if (!frame.matches("\\(.*:[0-9][0-9][0-9]:(\\.|\\+)\\)"))
                throw new ProtocolException("frame format error");

            String messageSegment = frame.substring(1, frameLength-7);
            String checksumSegment = frame.substring(frameLength-6, frameLength-3);

            terminal.printlnDiag("  messageSegment: " + messageSegment);

            int colonCount = 0;
            for (int i = 0; i < messageSegment.length(); i++) {
                if (messageSegment.charAt(i) == ':')
                    colonCount++;
            }
            if((colonCount & 1) != 0)
                throw new ProtocolException("frame format error: odd number of colons");

            messageSegment = messageSegment.replaceAll("::", ":");

            int checksum = 0;
            for (int c = 0; c < messageSegment.length(); c++)
                checksum += messageSegment.charAt(c);

            String checksumStr = "" + checksum;
            for (int i = checksumStr.length(); i < 3; i++)
                checksumStr = "0" + checksumStr;
            checksumStr = checksumStr.substring(checksumStr.length() - 3);

            if (!checksumSegment.equals(checksumStr))
                throw new ProtocolException("checksum error: expected " + checksumStr);

            message += messageSegment;
            eom = frame.charAt(frameLength-2) == '.';
        } while (!eom);


        // YOUR CODE SHOULD FINISH HERE --------------------------------
        // No changes are needed to the statements below

        // Return message

        if (message == null)
            terminal.printlnDiag("  receiveMessage returning null (end of input stream)");
        else
            terminal.printlnDiag("  receiveMessage returning \"" + message + "\"");
        return message;

    } // end of method receiveMessage

    // You may add private methods if you wish


} // end of class MessageReceiver

